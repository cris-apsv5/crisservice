package es.upm.dit.apsv.cris.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import es.upm.dit.apsv.cris.dao.PublicationDAO;
import es.upm.dit.apsv.cris.dao.PublicationDAOImplementation;
import es.upm.dit.apsv.cris.dataset.CSV2DB;
import es.upm.dit.apsv.cris.model.Publication;

class TestPublicationDAOImplementation {

	private Publication r;
	private PublicationDAO rdao;

	@BeforeAll
	static void dbSetUp() throws Exception {
		CSV2DB.loadPublicationsFromCSV("publications.csv");
	}

	@BeforeEach
	void setUp() throws Exception {
		rdao = PublicationDAOImplementation.getInstance();
		r = new Publication();
		r.setId("20564020");
		r.setTitle("El origen");
		r.setPublicationName("El orgigen de las Especies");
		r.setPublicationDate("30/11/2020");
		r.setAuthors("Javier");
		r.setCiteCount(3);
	}

	@Test
	void testCreate() {
		rdao.delete(r);
		rdao.create(r);
		assertEquals(r, rdao.read(r.getId()));
	}

	@Test
	void testRead() {
		rdao.create(r);
		assertEquals(r, rdao.read(r.getId()));
	}

	@Test
	void testUpdate() {
		String oldauth = r.getAuthors();
		r.setAuthors("Alejandro");
		rdao.update(r);
		assertEquals(r, rdao.read(r.getId()));
		r.setAuthors(oldauth);
		rdao.update(r);
	}
	



	@Test
	void testDelete() {
		rdao.delete(r);
		assertNull(rdao.read(r.getId()));
		rdao.create(r);
	}

	@Test
	void testReadAll() {
		assertTrue(rdao.readAll().size() > 75);
	}

}
